## Changes Done :
* Refactoring code.
* Separating controllers.
* Separating logic functions into services (virement services , compte services).
* Adding Model to DTO mappers (compte,virement...) and actually work with them.
* Implementing the `Versement` functionality.
* Creating data seeder.
* Creating Env app variables (MONTANT_MAXIMAL , ENV ...).
* Add Gender model and link it to User.
* Solving logic errors (account can be sender and reciever at the same time , allow transaction even if there's no funds left).
* Using meaningful names while working with variables.
* Work with Big Decimal (and not casting it to int and call it a day).

## How to use 
To build the projet you will need : 
* Java 11+ 
* Maven

Build command : 
```
mvn clean install
```

Run command : 
```
./mvnw spring-boot:run 
## or use any prefered method (IDE , java -jar , docker .....)
```

**Don't forget to check out dev if you're not in it by default.**
