package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "/comptes")
public class CompteController {
    @Autowired
    CompteService compteService;

    @GetMapping("lister_comptes")
    List<CompteDto> loadAllCompte() {
        List<Compte> comptes = compteService.getAllComptes();

        return CompteMapper.mapAll(comptes);
    }
}
