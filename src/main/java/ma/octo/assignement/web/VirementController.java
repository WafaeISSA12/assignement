package ma.octo.assignement.web;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.VirementService;
import ma.octo.assignement.validations.VirementDtoValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController(value = "/virements")
class VirementController {
    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    private VirementService virementService;
    @Autowired
    private VirementDtoValidation virementDtoValidation;

    @GetMapping("lister_virements")
    List<VirementDto> loadAll() {
        List<Virement> virements = virementService.getAllVirements();
        return VirementMapper.mapAll(virements);
    }

    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Map<String,String>> createTransaction(@RequestBody VirementDto virementDto) {
        Map<String,String> responseBody = new HashMap<>();

        try{
            virementDtoValidation.validate(virementDto);
        }catch (Exception e){
            LOGGER.error(e.getMessage());
            responseBody.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(responseBody);
        }

        virementService.createVirement(virementDto);

        LOGGER.info("Transaction Done Successfully.");
        responseBody.put("message","Transaction Done Successfully.");
        return new ResponseEntity<>(responseBody,HttpStatus.CREATED);
    }
}
