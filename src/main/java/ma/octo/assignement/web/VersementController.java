package ma.octo.assignement.web;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.validations.VersementDtoValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController(value = "/versements")
public class VersementController {
    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    VersementService versementService;
    @Autowired
    VersementDtoValidation versementDtoValidation;

    @GetMapping("lister_versement")
    List<VersementDto> loadAll(){
        return VersementMapper.mapAll(versementService.getAllVersements());
    }

    @PostMapping("/executerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Map<String,String>> createTransaction(@RequestBody VersementDto versementDto) {
        Map<String,String> responseBody = new HashMap<>();

        try{
            versementDtoValidation.validate(versementDto);
        }catch (Exception e){
            LOGGER.error(e.getMessage());
            responseBody.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(responseBody);
        }

        versementService.createVersement(versementDto);

        LOGGER.info("Versement Done Successfully.");
        responseBody.put("message","Versement Done Successfully.");
        return new ResponseEntity<>(responseBody,HttpStatus.CREATED);
    }
}
