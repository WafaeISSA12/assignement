package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

import java.util.ArrayList;
import java.util.List;

public class VirementMapper {

    public static VirementDto map(Virement virement) {
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());

        return virementDto;

    }

    public static List<VirementDto> mapAll(List<Virement> virements){
        List<VirementDto> virementDtos = new ArrayList<>();

        for(Virement virement:virements){
            virementDtos.add(map(virement));
        }

        return virementDtos;
    }
}
