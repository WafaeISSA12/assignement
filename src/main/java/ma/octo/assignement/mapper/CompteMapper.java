package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;

import java.util.ArrayList;
import java.util.List;

public class CompteMapper {
    public static CompteDto map(Compte compte){
        return new CompteDto(compte.getNrCompte(), compte.getRib(), compte.getSolde(),UtilisateurMapper.map(compte.getUtilisateur()));
    }

    public static List<CompteDto> mapAll(List<Compte> comptes){
        List<CompteDto> compteDtos = new ArrayList<>();

        for(Compte compte:comptes){
            compteDtos.add(map(compte));
        }
        return compteDtos;
    }
}
