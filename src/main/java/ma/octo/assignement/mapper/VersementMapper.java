package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

import java.util.ArrayList;
import java.util.List;

public class VersementMapper {
    public static VersementDto map(Versement versement){
        return new VersementDto(versement.getMontantVersement(),versement.getDateExecution(),versement.getNomEmetteur(),versement.getPrenomEmetteur(),versement.getCompteBeneficiaire().getRib(),versement.getMotifVersement());
    }

    public static List<VersementDto> mapAll(List<Versement> versements){
        List<VersementDto> versementsDtos = new ArrayList<>();

        for (Versement versement:versements){
            versementsDtos.add(map(versement));
        }
        return versementsDtos;
    }
}
