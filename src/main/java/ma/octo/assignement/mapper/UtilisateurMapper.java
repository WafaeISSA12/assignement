package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;

import java.util.ArrayList;
import java.util.List;

public class UtilisateurMapper {

    public static UtilisateurDto map(Utilisateur utilisateur){
        return new UtilisateurDto(utilisateur.getUsername(), utilisateur.getFirstname(), utilisateur.getLastname(), utilisateur.getGender().getLabel(), utilisateur.getBirthdate());
    }

    public static List<UtilisateurDto> mapAll(List<Utilisateur> utilisateurs){
        List<UtilisateurDto> utilisateurDtos = new ArrayList<>();

        for (Utilisateur utilisateur:utilisateurs){
            utilisateurDtos.add(map(utilisateur));
        }
        return utilisateurDtos;
    }
}
