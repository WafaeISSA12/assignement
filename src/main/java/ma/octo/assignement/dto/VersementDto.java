package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VersementDto {
    private BigDecimal montantVersement;
    private Date dateExecution;
    private String nomEmetteur;
    private String prenomEmetteur;
    private String ribBeneficiaire;
    private String motifVersement;
}
