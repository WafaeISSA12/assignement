package ma.octo.assignement.seeders;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Gender;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.GenderRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

@Component
public class MockDataSeeder implements CommandLineRunner {
    @Value("${ENV}")
    private String ENV;
    @Value("${PROD}")
    private String PROD;

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private GenderRepository genderRepository;

    @Override
    public void run(String... strings) throws Exception {
        // DO not push Mock data if the application is in prod
        if(ENV.equals(PROD))return;

        Gender male =new Gender();
        male.setLabel("Male");

        genderRepository.save(male);

        Gender female =new Gender();
        female.setLabel("Female");

        genderRepository.save(female);


        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender(male);
        utilisateur1.setBirthdate(new Date());

        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender(female);
        utilisateur1.setBirthdate(new Date());

        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);

        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);

        compteRepository.save(compte2);

        Virement v = new Virement();
        v.setMontantVirement(BigDecimal.TEN);
        v.setCompteBeneficiaire(compte2);
        v.setCompteEmetteur(compte1);
        v.setDateExecution(new Date());
        v.setMotifVirement("Assignment 2021");

        virementRepository.save(v);
    }
}
