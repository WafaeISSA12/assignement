package ma.octo.assignement.validations;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class VirementDtoValidationImpl implements VirementDtoValidation{
    @Value("${MONTANT_MAXIMAL}")
    private int MONTANT_MAXIMAL;
    @Value("${MONTANT_MINIMAL}")
    private int MONTANT_MINIMAL;

    @Autowired
    private CompteService compteService;



    public void validate(VirementDto virementDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        Compte compteEmetteur = compteService.getByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBenificiaire = compteService.getByNrCompte(virementDto.getNrCompteBeneficiaire());
        if (compteEmetteur == null || compteBenificiaire == null) {
            throw new CompteNonExistantException("Compte Non existant");
        }
        if(virementDto.getNrCompteEmetteur().equals(virementDto.getNrCompteBeneficiaire())){
            throw new TransactionException("Vous ne pouvez pas être l'emetteur et le benificiaire en même temps");
        }
        if (virementDto.getMontantVirement() == null || virementDto.getMontantVirement().intValue() == 0) {
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().compareTo(new BigDecimal(MONTANT_MINIMAL)) < 0) {
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().compareTo(new BigDecimal(MONTANT_MAXIMAL)) > 0) {
            throw new CompteNonExistantException("Montant maximal de virement dépassé");
        }
        if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }
    }
}
