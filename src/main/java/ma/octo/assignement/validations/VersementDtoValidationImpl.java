package ma.octo.assignement.validations;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class VersementDtoValidationImpl  implements VersementDtoValidation{
    @Value("${MONTANT_MAXIMAL}")
    private int MONTANT_MAXIMAL;
    @Value("${MONTANT_MINIMAL}")
    private int MONTANT_MINIMAL;

    @Autowired
    private CompteService compteService;


    @Override
    public void validate(VersementDto versementDto) throws TransactionException, CompteNonExistantException {
        Compte compteBenificiaire = compteService.getByRib(versementDto.getRibBeneficiaire());
        if (compteBenificiaire == null) {
            throw new CompteNonExistantException("Compte Non existant");
        }
        if (versementDto.getMontantVersement().equals(null) || versementDto.getMontantVersement().intValue() == 0) {
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().compareTo(new BigDecimal(MONTANT_MINIMAL)) < 0) {
            throw new TransactionException("Montant minimal de versement non atteint");
        } else if (versementDto.getMontantVersement().compareTo(new BigDecimal(MONTANT_MAXIMAL)) > 0) {
            throw new CompteNonExistantException("Montant maximal de versement dépassé");
        }
        if (versementDto.getMotifVersement().length() < 0) {
            throw new TransactionException("Motif vide");
        }
    }
}
