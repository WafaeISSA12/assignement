package ma.octo.assignement.validations;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface VersementDtoValidation {
    void validate(VersementDto versementDto) throws TransactionException, CompteNonExistantException;
}
