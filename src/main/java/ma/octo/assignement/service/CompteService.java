package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;

import java.math.BigDecimal;
import java.util.List;

public interface CompteService {
    List<Compte> getAllComptes();
    Compte getByNrCompte(String numeroCompte);
    Compte getByRib(String rib);

    void addToSolde(String nrCompte,BigDecimal amount);
    void addToSoldeByRib(String rib,BigDecimal amount);
    void removeFromSolde(String nrCompte, BigDecimal amount);
}
