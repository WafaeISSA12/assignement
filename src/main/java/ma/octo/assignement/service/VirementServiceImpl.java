package ma.octo.assignement.service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.VirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VirementServiceImpl implements VirementService{
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private AutiService autiService;

    @Override
    public List<Virement> getAllVirements() {
        return virementRepository.findAll();
    }

    @Override
    public void createVirement(VirementDto virementDto) {
        compteService.removeFromSolde(virementDto.getNrCompteEmetteur(),virementDto.getMontantVirement());
        compteService.addToSolde(virementDto.getNrCompteBeneficiaire(),virementDto.getMontantVirement());

        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(compteService.getByNrCompte(virementDto.getNrCompteBeneficiaire()));
        virement.setCompteEmetteur(compteService.getByNrCompte(virementDto.getNrCompteEmetteur()));
        virement.setMontantVirement(virementDto.getMontantVirement());

        virementRepository.save(virement);

        autiService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString());
    }
}
