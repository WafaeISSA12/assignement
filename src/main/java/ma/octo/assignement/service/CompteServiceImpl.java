package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CompteServiceImpl implements CompteService {
    @Autowired
    private CompteRepository compteRepository;

    @Override
    public List<Compte> getAllComptes() {
        return compteRepository.findAll();
    }

    @Override
    public Compte getByNrCompte(String numeroCompte) {
        return compteRepository.findByNrCompte(numeroCompte);
    }

    @Override
    public Compte getByRib(String rib){
        return compteRepository.findByRib(rib);
    }

    @Override
    public void addToSolde(String nrCompte, BigDecimal amount) {
        Compte compte = compteRepository.findByNrCompte(nrCompte);
        compte.setSolde(compte.getSolde().add(amount));
        compteRepository.save(compte);
    }
    @Override
    public void addToSoldeByRib(String rib, BigDecimal amount) {
        Compte compte = compteRepository.findByRib(rib);
        compte.setSolde(compte.getSolde().add(amount));
        compteRepository.save(compte);
    }

    @Override
    public void removeFromSolde(String nrCompte, BigDecimal amount) {
        Compte compte = compteRepository.findByNrCompte(nrCompte);
        compte.setSolde(compte.getSolde().subtract(amount));
        compteRepository.save(compte);
    }
}
