package ma.octo.assignement.service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

import java.util.List;

public interface VirementService {
    List<Virement> getAllVirements();

    void createVirement(VirementDto virement);
}
