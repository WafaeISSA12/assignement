package ma.octo.assignement.service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.repository.VersementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class VersementServiceImpl implements VersementService{
    @Autowired
    VersementRepository versementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private AutiService autiService;

    @Override
    public List<Versement> getAllVersements() {
        return versementRepository.findAll();
    }

    @Override
    public void createVersement(VersementDto versementDto) {
        compteService.addToSoldeByRib(versementDto.getRibBeneficiaire(),versementDto.getMontantVersement());

        Versement versement = new Versement();
        versement.setNomEmetteur(versementDto.getNomEmetteur());
        versement.setPrenomEmetteur(versementDto.getPrenomEmetteur());
        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setCompteBeneficiaire(compteService.getByRib(versementDto.getRibBeneficiaire()));
        versement.setDateExecution(new Date());

        versementRepository.save(versement);

        autiService.auditVersement("Versement depuis " + versementDto.getNomEmetteur()+" "+versementDto.getPrenomEmetteur() + " vers " + versementDto
                .getRibBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());
    }
}
