package ma.octo.assignement.service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

import java.util.List;

public interface VersementService {
    List<Versement> getAllVersements();

    void createVersement(VersementDto versementDto);
}
